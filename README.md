# gitlab-sandbox

Configure and run a GitLab sandbox instance, including:

- A Patroni cluster behind a PgBouncer. The cluster is 1 node by default but
  more nodes can be added (see
  [Adding more patroni nodes](#adding-more-patroni-nodes)).

## Setting up

We use [Kitchen](https://kitchen.ci/) to automate the creation of a local cluster.
We use the [kitchen-google](https://github.com/test-kitchen/kitchen-google) driver
to provision VMs. Before starting, make sure you've configured [Authentication
for GCP](https://github.com/test-kitchen/kitchen-google#authentication-and-authorization).
Also make sure to set [SSH keys](https://github.com/test-kitchen/kitchen-google#ssh-keys)
to be able to connect to the VMs. The username of those SSH keys should match
the <gcp-username> on the commands below.

To create the cluster, run the following (add `-c <count>` after `create`/`converge` to
run the operation in parallel):

```
$ export GITLAB_SANDBOX_GCP_PROJECT=<gcp-project-id> GITLAB_SANDBOX_GCP_REGION=<gcp-region> GITLAB_SANDBOX_GCP_USERNAME=<gcp-username>
$ kitchen create
$ export GITLAB_SANDBOX_GCP_INTERFACE_NAME=$(kitchen exec patroni-1 --no-color -c "ip -o -4 addr show | grep 10.142.0.5 | cut -f2 -d' '" | tail -1)
$ kitchen converge patroni
$ kitchen exec patroni -c 'sudo systemctl start patroni'
$ kitchen converge pgbouncer
$ kitchen login patroni-1-ubuntu-1604
patroni-1-ubuntu-1604$ sudo gitlab-psql -d postgres
ALTER ROLE gitlab WITH login;
ALTER ROLE gitlab superuser;
CREATE EXTENSION btree_gist;
CREATE EXTENSION pg_trgm;
create database gitlabhq_production;
ALTER DATABASE gitlabhq_production OWNER TO gitlab;
GRANT ALL PRIVILEGES ON DATABASE gitlabhq_production TO gitlab;
\c gitlabhq_production
CREATE OR REPLACE FUNCTION public.pg_shadow_lookup(in i_username text, out username text, out password text)
RETURNS record AS $$
BEGIN
    SELECT usename, passwd FROM pg_catalog.pg_shadow
    WHERE usename = i_username INTO username, password;
    RETURN;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;
REVOKE ALL ON FUNCTION public.pg_shadow_lookup(text) FROM public, pgbouncer;
GRANT EXECUTE ON FUNCTION public.pg_shadow_lookup(text) TO pgbouncer;
$ kitchen converge gitaly
$ kitchen converge gitlab
```

## Connecting

Try port-forwarding:

```
gcloud compute ssh <gitlab machine name> --project $GITLAB_SANDBOX_GCP_PROJECT -- -L 8080:localhost:80
```

## Adding more patroni nodes

- Add as many `patroni-<n>` sections as needed to [kitchen.yml](./kitchen.yml).
  Make sure to assign a unique unused internal IP to each of them.
- Add the new IPs to `gitlab_consul.cluster_nodes`
- Set `consul.config.bootstrap_expect` to the new total of patroni nodes

## Gotchas

### On preemptible machines

We're using ["preemptible"](https://cloud.google.com/compute/docs/instances/preemptible)
VMs for cost-efficiency. These machines might be stopped by GCP without warning
but this should be ok for the purposes of ethereal sandboxing this kitchen setup
is aimed at. However, if you restart a stopped VM it might change its external
IP, making your kitchen state obsolete. You can fix this by deleting and
recreating the kitchen cluster (or manually update the `hostname`s on your
kitchen state files if you are so inclined).

### On pgbouncer DNS failures

There's a know issue where a VM starting/restarting may cause pgbouncer to fail
to resolve consul DNS records (see https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10673).
`sudo systemctl restart pgbouncer` should solve the issue.
